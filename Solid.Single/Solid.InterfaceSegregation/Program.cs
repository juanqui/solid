﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.InterfaceSegregation
{
    public interface IHeroe
    {
        void Run();
        void Punch();
    }
    public interface IFlyingHeroe
    {
        void Fly();
    }

    public class Superman : IHeroe, IFlyingHeroe
    {
        public void Run() { }

        public void Punch() { }

        public void Fly() { }
    }
    public class Batman : IHeroe
    {
        public void Punch() { }

        public void Run() { }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Review code, please");
            Console.ReadKey();
        }
    }
}

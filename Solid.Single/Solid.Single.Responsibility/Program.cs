﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.Single.Responsibility
{
    //One class should be responsible for one task.
    public class Employee
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
    }
    public class AddEmployee
    {
        public Employee Emp = new Employee();
        public AddEmployee()
        {
            Emp.EmployeeId = 1;
            Emp.EmployeeName = "Fabricio";
        }
    }
    public class ShowEmployee
    {
        public AddEmployee AEmployee = new AddEmployee();
        
        public void ReportEmployee()
        {
            Console.WriteLine("{0} employee: {1}", AEmployee.Emp.EmployeeId, AEmployee.Emp.EmployeeName);
            Console.ReadKey();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            ShowEmployee se = new ShowEmployee();
            se.ReportEmployee();
        }
    }
}

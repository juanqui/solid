﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.DependencyInv
{
    public interface IOperation
    {
        void Execute();
    }
    public class Operation1:IOperation
    {
        public void Execute()
        {
            Console.WriteLine("An action from Operation1");
        }
    }
    public class Operation2:IOperation
    {
        public void Execute()
        {
            Console.WriteLine("An action from Operation2");
        }
    }
    public class Executable
    {
        IOperation op;
        public void ExecuteOperation(IOperation operation)
        {
            op = operation;
            op.Execute();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Executable exec = new Executable();
            Operation1 op = new Operation1();
            exec.ExecuteOperation(op);

            Operation2 op2 = new Operation2();
            exec.ExecuteOperation(op2);

            Console.ReadKey();
        }
    }
}

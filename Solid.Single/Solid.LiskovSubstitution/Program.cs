﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.LiskovSubstitution
{   //is just a extension of the Open Close Principle 
    class Rectangle
    {
        protected int height;
        protected int width;

        public virtual void SetHeight(int h)
        {
            height = h;
        }

        public virtual void SetWidth(int w)
        {
            width = w;
        }

        public int CalcArea()
        {
            return width * height;
        }
    }

    class Square : Rectangle
    {
        public override void SetHeight(int h)
        {
            height = width = h;
        }

        public override void SetWidth(int w)
        {
            height = width = w;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle rect = new Rectangle();
            rect.SetHeight(5);
            rect.SetWidth(10);

            Console.WriteLine("Rectangle area {0}", rect.CalcArea());

            Rectangle rect2 = new Square();
            rect2.SetHeight(5);
            rect2.SetWidth(10);
            //should be 50, LSP fail.
            Console.WriteLine("Rectangle area {0}", rect2.CalcArea());

            Console.ReadKey();
        }
    }
}

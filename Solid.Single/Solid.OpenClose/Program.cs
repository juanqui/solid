﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.OpenClose
{   //Open for extension but closed by closed for modification
    public abstract class Salary
    {
        public abstract decimal CalculateSal(string emptype);
    }
    class Nurse:Salary
    {
        public override decimal CalculateSal(string emptype)
        {
            return 2500;
        }
    }
    class Teacher:Salary
    {
        public override decimal CalculateSal(string emptype)
        {
            return 2000;
        }
    }
    class Peon : Salary
    {
        public override decimal CalculateSal(string emptype)
        {
            return 1000;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Nurse n = new Nurse();
            Teacher t = new Teacher();
            Peon p = new Peon();
            Console.WriteLine("nurse {0}, teacher {1}, peon {2}", n.CalculateSal("xyz"), t.CalculateSal("xyz"), p.CalculateSal("xyz"));
            Console.ReadKey();
        }
    }
}
